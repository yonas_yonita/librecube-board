EESchema Schematic File Version 4
LIBS:librecube_pcb_template-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H2
U 1 1 5C2F526D
P 9400 5100
F 0 "H2" H 9450 6517 50  0000 C CNN
F 1 "Conn_02x26_Odd_Even" H 9450 6426 50  0000 C CNN
F 2 "LibreCube PCB Template:Samtec_HLE-126-02-xx-DV-TE_2x26_P2.54mm_Horizontal" H 9400 5100 50  0001 C CNN
F 3 "~" H 9400 5100 50  0001 C CNN
	1    9400 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1050 9200 1050
Wire Wire Line
	10100 1050 9700 1050
Text Label 8800 1050 0    50   ~ 0
CAN_A_L
Text Label 9800 1050 0    50   ~ 0
CAN_A_H
Wire Wire Line
	10100 2550 9700 2550
Wire Wire Line
	8800 2550 9200 2550
Text Label 8800 2550 0    50   ~ 0
CHARGE
Text Label 9800 2550 0    50   ~ 0
CHARGE
Wire Wire Line
	10100 3900 9700 3900
Wire Wire Line
	10100 4000 9700 4000
Text Label 9750 3900 0    50   ~ 0
CAN_B_L
Text Label 9750 4000 0    50   ~ 0
CAN_B_H
$Comp
L Connector_Generic:Conn_02x26_Odd_Even H1
U 1 1 5C4C195F
P 9400 2250
F 0 "H1" H 9450 3667 50  0000 C CNN
F 1 "Conn_02x26_Odd_Even" H 9450 3576 50  0000 C CNN
F 2 "LibreCube PCB Template:Samtec_HLE-126-02-xx-DV-TE_2x26_P2.54mm_Horizontal" H 9400 2250 50  0001 C CNN
F 3 "~" H 9400 2250 50  0001 C CNN
	1    9400 2250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
